header1 = 'User'
header2 = 'Days_Active'
header3 = 'Time_Spent_on_Site_h'
User = ["Katie", "Ben", "Adam", "Jodie"]
Days_Active = ["10", "21", "1", "2"]
Time_Spent_on_Site_h = ["27.30", "89.12", "0.50", "6.00"]

header_row = f"| {header1:20} | {header2:20} | {header3:20}"
print(header_row)

print("-" * len(header_row))

account1 = f"| {User[0]:20} | {Days_Active[0]:20} | {Time_Spent_on_Site_h[0]:20}"
print(account1)

account2 = f"| {User[1]:20} | {Days_Active[1]:20} | {Time_Spent_on_Site_h[1]:20}"
print(account2)

account3 = f"| {User[2]:20} | {Days_Active[2]:20} | {Time_Spent_on_Site_h[2]:20}"
print(account3)

account4 = f"| {User[3]:20} | {Days_Active[3]:20} | {Time_Spent_on_Site_h[3]:20}"
print(account4)

